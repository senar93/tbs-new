1) prima dello schieramento delle unit� vengono eseguite le abilit� con i tag [...] presenti nella lista delle abilit� dell armata
2) fase di schieramento, in questa fase entrambi i giocatori schierano le proprie unit�
	normalmente questa fase viene eseguita contemporaneamente da entrambi i giocatori e i giocatori non conoscono la posizione, il numero e il tipo di unit� avversarie
	durante questa fase vengono eseguite le abilit� con tag [...] dell armata e le abilt� con tag [...] delle unit� che si stanno schierando
3) immediatamente dopo lo schieramento (e prima dell inizio della battaglia vera e propria) vengono eseguite le abilit� con tag [...] delle armate e le abilit� con tag [...] dei gruppi
4) viene creata e popolata una lista (TurnList) in cui sono inserite tutti i gruppi giocanti secondo un certo ordine, vengono eseguite le abilit� delle armate con tag [...] e le abilit� dei gruppi con tag [...]

5) inizio turno, agisce la prima unit� di TurnList, vengono attivate le abilit� con tag [...] dei gruppi e con tag [...] del gruppo che sta agendo
6) se il gruppo che sta agendo pu� agire il proprietario del gruppo sceglie l'azione da compiere tra quelle possibili, a seconda delle azioni vengono eseguite fasi diverse
   altrimenti viene aggiornata TurnList e si riparte dalla fase 5
	durante il turno di un certo giocatore quel giocatore pu� utilizare, in aggiunta alle azioni permesse dall gruppo che sta agendo in quel momento, qualsiasi abilit� attivabile con tag [...] presente nella lista delle abilit� dell armata 

7) ci sono diverse fasi 7 possibili inoltre durante la fase 7 pu� avvenire un altra fase 7 e cosi via (viene eseguita l'ultima fase che non ne richiamata altre, poi quella prima ecc)
7a) movimento [...]
7b) danneggiare qualcosa [...]
7c) curare/resuscitare qualcosa [...]
7d) modificare la lista di abilit� di qualcosa (aggiungere, eliminare, variare in qualche modo) [...]
[...]

8) al termine di ogni possibile fase 7 viene aggiornata TurnList e si ritorna alla fase 5


