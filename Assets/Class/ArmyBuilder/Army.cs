﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//oggetto che contiene tutte le informazioni sul esercito creato, le informazioni contenute in questo oggetto sono quelle usate per schierare l'esercito in battaglia
public class Army : MonoBehaviour {
	
	public EnumRaceType race;
	public AbsHero hero;
	public List<UnityArmy> unityList;

}
