﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class UnityArmy {
	public int number;
	public UnityBase unity;
	public AuxiliaryBase[] auxiliaryList;

	public List<AbsAbility> GetAllAbility() {
		List<AbsAbility> tmp = new List<AbsAbility>();
		if (unity.abilityList != null) tmp.AddRange(unity.abilityList);
		if (auxiliaryList != null)
			for (int i = 0; i < auxiliaryList.Length; i++)
				if (auxiliaryList[i] != null)
					tmp.AddRange(auxiliaryList[i].abilityList);

		return tmp;
	}
}