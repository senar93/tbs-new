﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//ci sarà indicativamente un implementazione di HeroBattle per ogni possibile valore contenuto in EnumHeroDeployMode
public abstract class AbsHero : MonoBehaviour {
	public EnumHeroDeployMode heroDeployMode;
	
	private List<AbsAbility> abilityList;
	private List<Item> itemList;
	//non ci sarà alcun puntatore ai talenti poichè le abilità fornite dai talenti verranno inserite in abilityList quando viene creato quest oggetto

	//restituisce tutte le abilità possedute dall eroe
	public List<AbsAbility> GetAllAbility() {
		List<AbsAbility> tmpAllAbility = new List<AbsAbility>();

		if(abilityList != null)
			tmpAllAbility.AddRange(abilityList);

		tmpAllAbility.AddRange( GetItemAbility() );

		return tmpAllAbility;
	}
	//resitiuisce le abilità fornite dagli item
	private List<AbsAbility> GetItemAbility() {
		List<AbsAbility> tmpItemAbility = new List<AbsAbility>();

		if(itemList != null)
			for (int i = 0; i < itemList.Count; i++)
				if(itemList[i] != null && itemList[i].abilityGive != null)
					tmpItemAbility.AddRange(itemList[i].abilityGive);

		return tmpItemAbility;
	}


	public void SetAbilityList(List<AbsAbility> list) {
		abilityList = list;
	}
	public void SetItemList(List<Item> list) {
		itemList = list;
	}


}


