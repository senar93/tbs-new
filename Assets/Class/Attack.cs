﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Attack {

	public float damageMin;
	public float damageMax;
	public int rangeMin;
	public int rangeMax;
	public EnumAttackType attackType;
	public EnumDamageType damageType;
	public bool attackMoveEnable = false;
	




}
