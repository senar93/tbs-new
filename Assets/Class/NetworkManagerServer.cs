﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkManagerServer : NetworkManager
{
	[SerializeField] private long connectionNumberTotal = 0;

	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
	{
		//base.OnServerAddPlayer(conn, playerControllerId);
		GameObject player = (GameObject)GameObject.Instantiate(playerPrefab);
		NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
		connectionNumberTotal++;

		player.name = "Player " + connectionNumberTotal;
		player.GetComponent<NetworkPlayer>().Rpc_RenamePlayer(player.name);
	}

}
