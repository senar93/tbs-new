﻿using UnityEngine;
using System.Collections;

public class UnityBase : MonoBehaviour {

	public AbsAbility[] abilityList;
	public AbsObjectRequisite requisite;
	public int auxiliaryMaxNumber = 1;
	public AuxiliaryBase[] auxiliaryAllowed; 


	/*
	public AbsAbility[] GetAbilityList() {
		return abilityList;
	}
	public AuxiliaryBase[] GetAuxiliaryAllowedList() {
		return auxiliaryAllowed;
	}
	public int GetAuxiliaryMaxNumber() {
		return auxiliaryMaxNumber;
	}
	public AbsObjectRequisite GetUnityRequisite() {
		return requisite;
	}
	*/

}
