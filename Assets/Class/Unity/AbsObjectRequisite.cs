﻿using UnityEngine;
using System.Collections;

//classe astratta, le cui implementazioni contengono il costo per unità, il numero massimo di unità di quel tipo permesse e qualsiasi altro requisito per inserire l'unità nell esercito
public abstract class AbsObjectRequisite : MonoBehaviour {
	public float UnityPointCost;
	public float HeroPointCost;
	//public int maxNumberAllowed;		//andrà inserito solo nelle implementazioni della classe legate alle unità

}
