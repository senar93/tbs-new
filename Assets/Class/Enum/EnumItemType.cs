﻿public enum EnumItemType
{
	consumable = 1,

	oneHand = 2,
	twoHand = 3,
	ammo = 4,

	head = 5,               //testa
	shoulder = 6,           //spalle
	chest = 7,              //torso
	legs = 8,               //game
	belt = 9,               //cintura / fiancali
	wrist = 10,             //braccia
	hand = 11,              //mani
	feet = 12,              //piedi
	neck = 13,              //collo
	cloaks = 14,            //mantello
	ear = 15,               //orecchie
	finger = 16,            //dita


	generic = 0
}