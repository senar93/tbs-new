﻿//indica il tipo di selezioni disponibili nella funziona Select del oggetto Map
public enum EnumMapSelectNodeType {
	
	row = 3,
	circle = 4,
	cone = 5,
	
	
	all = 0,
	pathfinding = 1,
	nodeCoords = 2,
	listOfCoords = 3
	
}
