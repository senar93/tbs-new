﻿// è il tipo di dati con cui sono classificate tutte le risorse possibili consumabili in una battaglia tattica
public enum EnumResourceType {
	//risorse legate al sistema di calcolo turno (range 1 -> 99)
	initiative = 1,

	//risorse generiche (range 100 -> N)
	health = 100,
	mana = 101,
	stamina = 102,

	//valore particolare
	none = 0
}