﻿//indica in che fase del calcolo viene eseguita l'abilità
public enum EnumApplicationTagType
{
	//base resource value
	attackDamageBase = 3,

	#region HELATH
	//a tutte le abilità con questi tag viene passato in target un oggetto di tipo StatCalcTarget (escluse quelle con healthBase)
	// l'oggetto di tipo StatCalcTarget ha il parametro di tipo targetStat di tipo *** float *** durante questo calcolo
	//il valore restituito da tutte le abilità con questo tag DEVE essere un float
	
	// restituisce il valore di base su cui calcolare l'health
	// se un gruppo ha più di un abilità con questo tag solo la prima viene considerata
	healthMaxBase = 1,
	//le funzioni con questo tag restituiscono un incremento fisso di health 
	healthMaxCalcStart = 101,
	healthMaxCalcInc = 102,
	//le funzioni con questo tag restituiscono un incremento in percentuale di health
	healthMaxCalcIncXC = 103,
	//le funzioni con questo tag restituiscono un incremento fisso di health
	healthMaxCalcFinal = 104,

	//il valore finale del calcolo degli hp massimi deve essere maggiore di 0, altrimenti gli hp massimi dell unità vengono cosiderati il valore minimo possibile 
	
	healthActBase = 199,			//se il gruppo possiede degli hp che vengono calcolati in automatico è necessario associargli una BaseAbility (non un abilità qualsiasi) con questo tag
	#endregion
	#region ARMOR
	armorBase = 2,
	
	armorAllCalcStart = 201,
	armorValueCalcStart = 202,
	armorValueCalcInc = 203,
	armorValueCalcIncXC = 204,
	armorValueCalcFinal = 205,
	armorTypeCalc = 206,
	armorAllCalcFinal = 207,
	#endregion



	//valore particolare
	none = 0
}