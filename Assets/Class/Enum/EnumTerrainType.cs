﻿public enum EnumTerrainType { 
	ground = 1,
	grass = 2,
	grassTall = 3,
	sand = 4,
	waterLow = 5,
	waterDeep = 6,
	sky = 7,
	magma = 8,
	street = 9,
	tiled = 10,
	rock = 11,

	none = 0
}