﻿//modo di sferrare l'attacco
public enum EnumAttackType {

	melee = 1,
	ranged = 2,
	bySpell = 3,
	collateral = 4,             //es. danni ad area causati da un attacco che non ha come target l'unità

	none = 0
}