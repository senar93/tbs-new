﻿using UnityEngine;
using System.Collections;


// generica abilità, della quale sono fornite 3 implementazioni: BaseAbility, SpecificAbility e BattleAbility
public abstract class AbsAbility : MonoBehaviour
{

	[SerializeField] protected int priority;
	[SerializeField] public bool canBeRemove;                       //indica se l'abilità può essere eliminata dalla lista delle abilità del gruppo

	#region ABSTRACT FUNCTION
	// Effect(...) è la funzione che applica l'effetto dell abilità, qualunque esso sia, l'effetto varia da abilità a abilità ed il suo codice è scritto nelle varie implementazioni di AbsAbilityLinkedEffect 
	// "target" contiene il/i target della abilità, sarà l'abilità stessa a dover capire il tipo di target (se è un array, una lista e di che cosa)						//
	// "sender" contiene il possessore dell abilità, vale lo stesso ragionamento che per il target
	public abstract object Effect(object target = null,
								  AbilitySender sender = null,
								  AbsEventTagType eventTag = null,
								  AbsAbilityValue baseAbilityValue = null,
								  AbsAbilityValue specificAbilityValue = null,
								  AbsAbilityValue[] battleAbilityValue = null,
								  string optionalParameter = "");

	public abstract AbsEventTagType GetEventTagType(object target = null,
													AbilitySender sender = null,
													AbsAbilityValue baseAbilityValue = null,
													AbsAbilityValue specificAbilityValue = null,
													AbsAbilityValue[] battleAbilityValue = null,
													string optionalParameter = "");

	// GetPriority(...) restituisce la priorità associata all abilità
	//restituisce la base ability o specific ability finale della catena
	public abstract AbsAbility GetFinalAbility();
	public abstract float GetPriority(string optionalParameter = "");
	public abstract EnumApplicationTagType GetApplicationTag(string optionalParameter = "");
	public abstract bool IsPassive();
	#endregion


}