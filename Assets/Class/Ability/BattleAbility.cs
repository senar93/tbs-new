﻿using UnityEngine;
using System.Collections;
using System;


// implementazione di AbsAbility
// implementa le abilità specifiche di una singola battaglia in caso ne vadano modificati i parametri o altro, si alloca un oggetto di questo tipo e si modifica quello
// contiene:
//			 - il un puntatore a una classe di tipo AbsAbility, che contiene il link all abilità modificata
//			 - un puntatore a una classi di tipo AbsSpecificAbilityValue, contenente i parametri specifici dell abilità
public class BattleAbility : AbsAbility {

	public bool hasCustomPriority;
	[SerializeField] protected bool isLastOfChain;
	[SerializeField] protected BattleAbility previousBattleAbility;			//link alla battle ability precedente nella catena (l'abilità sucessiva invece è linkedAbility)

	public AbsAbility linkedAbility;                            //link all abilità modificata
	public AbsAbilityValue linkedBattleValue;					//link ai valori specifici dell abilità


	#region OVERRIDE
	public override float GetPriority(string optionalParameter = "") {
		if (hasCustomPriority) return priority;
		else return linkedAbility.GetPriority();
	}

	public override EnumApplicationTagType GetApplicationTag(string optionalParameter = "") {
		return linkedAbility.GetApplicationTag();
	}

	public override object Effect(object target = null,
								  AbilitySender sender = null,
								  AbsEventTagType eventTag = null,
								  AbsAbilityValue baseAbilityValue = null,
								  AbsAbilityValue specificAbilityValue = null,      //ignorato
								  AbsAbilityValue[] battleAbilityValue = null,
								  string optionalParameter = "")
	{
		if (linkedAbility != null)
		{
			// sovrascrittura battleAbilityValue
			if (battleAbilityValue == null)
			{
				battleAbilityValue = new AbsAbilityValue[1];
				battleAbilityValue[0] = linkedBattleValue;
			} else {
				AbsAbilityValue[] tmpAV = new AbsAbilityValue[battleAbilityValue.Length + 1];
				battleAbilityValue.CopyTo(tmpAV, 0);
				tmpAV[battleAbilityValue.Length] = linkedBattleValue;
				battleAbilityValue = tmpAV;
			}

			return linkedAbility.Effect(target, sender, eventTag, baseAbilityValue, specificAbilityValue, battleAbilityValue, optionalParameter);
		}
		else
			return null;
	}


	public override AbsEventTagType GetEventTagType(object target = null,
													AbilitySender sender = null,
													AbsAbilityValue baseAbilityValue = null,
													AbsAbilityValue specificAbilityValue = null,
													AbsAbilityValue[] battleAbilityValue = null,
													string optionalParameter = "")
	{
		if (linkedAbility != null)
		{
			// sovrascrittura battleAbilityValue
			if (battleAbilityValue == null)
			{
				battleAbilityValue = new AbsAbilityValue[1];
				battleAbilityValue[0] = linkedBattleValue;
			}
			else {
				AbsAbilityValue[] tmpAV = new AbsAbilityValue[battleAbilityValue.Length + 1];
				battleAbilityValue.CopyTo(tmpAV, 0);
				tmpAV[battleAbilityValue.Length] = linkedBattleValue;
				battleAbilityValue = tmpAV;
			}

			return linkedAbility.GetEventTagType(target, sender, baseAbilityValue, specificAbilityValue, battleAbilityValue, optionalParameter);
		}
		else
			return null;
	}

	public override AbsAbility GetFinalAbility() {
		return linkedAbility.GetFinalAbility();
	}
	public override bool IsPassive() {
		return linkedAbility.IsPassive();
	}

	#endregion

	//imposta il valore di previousBattleAbility e di isLastOfChain
	public void SetPreviousBattleAbility(BattleAbility newValue) {
		if (newValue != null)
			isLastOfChain = false;
		else
			isLastOfChain = true;

		previousBattleAbility = newValue;
	}

	public bool IsLastOfChain() {
		return isLastOfChain;
	}
	public BattleAbility GetPreviousBattleAbility() {
		return previousBattleAbility;
	}


}