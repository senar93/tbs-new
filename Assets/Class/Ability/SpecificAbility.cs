﻿using UnityEngine;
using System.Collections;
using System;


// implementazione di AbsAbility che implementa le varie abilità che fanno riferimento a un istanza di BaseAbility
// contiene:
//			 - un puntatore a una classe di tipo BaseAbility, al quale si fa riferimento per il funzionamento dell abilità
//			 - un puntatore a una classi di tipo AbsSpecificAbilityValue, contenente i parametri specifici dell abilità
public class SpecificAbility : AbsAbility {

	public bool hasCustomPriority;

	public BaseAbility linkedAbility;                               //link al abilità di base
	public AbsAbilityValue linkedSpecificValue;						//link ai valori specifici dell abilità



	#region OVERRIDE
	public override float GetPriority(string optionalParameter = "") {
		if (hasCustomPriority) return priority;
		else return linkedAbility.GetPriority();
	}

	public override EnumApplicationTagType GetApplicationTag(string optionalParameter = "") {
		return linkedAbility.GetApplicationTag();
	}

	public override object Effect(object target = null,
								  AbilitySender sender = null,
								  AbsEventTagType eventTag = null,
								  AbsAbilityValue baseAbilityValue = null,
								  AbsAbilityValue specificAbilityValue = null,      //ignorato
								  AbsAbilityValue[] battleAbilityValue = null,
								  string optionalParameter = "")
	{
		if (linkedAbility != null) {
			// sovrascrittura battleAbilityValue
			specificAbilityValue = linkedSpecificValue;

			return linkedAbility.Effect(target, sender, eventTag, baseAbilityValue, specificAbilityValue, battleAbilityValue, optionalParameter);
		}
		else
			return null;
	}

	public override AbsEventTagType GetEventTagType(object target = null,
													AbilitySender sender = null, 
													AbsAbilityValue baseAbilityValue = null, 
													AbsAbilityValue specificAbilityValue = null, 
													AbsAbilityValue[] battleAbilityValue = null, 
													string optionalParameter = "")
	{
		if (linkedAbility != null)
		{
			// sovrascrittura battleAbilityValue
			specificAbilityValue = linkedSpecificValue;

			return linkedAbility.GetEventTagType(target, sender, baseAbilityValue, specificAbilityValue, battleAbilityValue, optionalParameter);
		}
		else
			return null;
	}

	public override AbsAbility GetFinalAbility() {
		return this;
	}
	public override bool IsPassive() {
		return linkedAbility.IsPassive();
	}
	#endregion

}