﻿using UnityEngine;
using System.Collections;


//oggetto che contiene l'informazione su chi richiama il calcolo del abilità e con quale abilità
public class AbilitySender {
	public object abilityOwner;
	public AbsAbility ability;
}
