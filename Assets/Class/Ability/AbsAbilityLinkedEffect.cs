﻿using UnityEngine;
using System.Collections;


public abstract class AbsAbilityLinkedEffect : MonoBehaviour {
	//applica l'effetto dell abilità
	public abstract object Effect(object target = null,
								  AbilitySender sender = null,
								  AbsEventTagType eventTag = null,
								  AbsAbilityValue baseAbilityValue = null,
								  AbsAbilityValue specificAbilityValue = null,
								  AbsAbilityValue[] battleAbilityValue = null,
								  string optionalParameter = "");

	//restituisce l'evento generato dall utilizzo dell abilità,
	//di default restituisce null, se l'abilità genera un evento particolare va ridefinita all interno della classe che la implementa 
	public AbsEventTagType GetEventTagType(object target = null,
										   AbilitySender sender = null,
										   AbsAbilityValue baseAbilityValue = null,
										   AbsAbilityValue specificAbilityValue = null,
										   AbsAbilityValue[] battleAbilityValue = null,
										   string optionalParameter = "") {
		return null;
	}

	#region DINAMIC AbsAbility PARAMETER
	//qui verranno definite tutte le funzioni per ottenere i parametri contenuti in baseAbility,
	// di tutte loro va fornita un implementazione di default che deve essere interpretata come "utilizza i valori di default, non vengono mai ricalcolati"
	// è possibile ridefinire queste funzioni nelle varie implementazioni di LinkedEffect, in modo da variare dinamicamente i parametri delle abilità
	
	//gia implementate in AbsAbility
	
	//non ancora implementate in AbsAbility
	
	#endregion

}