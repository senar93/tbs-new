﻿using UnityEngine;
using System.Collections;

public class ArmorStandard_LinkedEffect : AbsAbilityLinkedEffect
{

	public Armor defaultValue;

	public override object Effect(object target = null,
								  AbilitySender sender = null,
								  AbsEventTagType eventTag = null,
								  AbsAbilityValue baseAbilityValue = null,
								  AbsAbilityValue specificAbilityValue = null,
								  AbsAbilityValue[] battleAbilityValue = null,
								  string optionalParameter = "")
	{

		//controlla che l'abilità sia stata richiamata da uno specific ability value
		if (specificAbilityValue != null)
			if (specificAbilityValue is Armor_AbilityValue)
				return (specificAbilityValue as Armor_AbilityValue).armor;

		//se avviene un errore di qualche tipo restituisce il valore minimo possibile di default
		return defaultValue;
	}

}
