﻿using UnityEngine;
using System.Collections;
using System;

//questa classe viene usata per la versione standard di healthMaxBase, heathMaxInc, [...] healthMaxFinal
//fornisce un incremento fisso alla statistica (fisso o percentuale che sia) oppure un valore di base
//nel caso di start e final fornisce un incremento fisso ( +value )
public class ReturnFloatValue_LinkedEffect : AbsAbilityLinkedEffect {

	public float defaultValue = 0;
	
	// target è di tipo "StatCalcTarget"
	// il parametro "target.targetStat" è di tipo float
	// il valore di restituito è di tipo float
	// l'abilità non prevede l'esistenza di alcuna possibile BattleAbility collegata ad essa, ne può funzionare da sola, è sempre collegata una SpecificAbility
	public override object Effect(object target = null, 
								  AbilitySender sender = null, 
								  AbsEventTagType eventTag = null, 
								  AbsAbilityValue baseAbilityValue = null, 
								  AbsAbilityValue specificAbilityValue = null, 
								  AbsAbilityValue[] battleAbilityValue = null, 
								  string optionalParameter = "")
	{

		//controlla che l'abilità sia stata richiamata da uno specific ability value
		if (specificAbilityValue != null){
			if (specificAbilityValue is FloatValue_AbilityValue)
				return (specificAbilityValue as FloatValue_AbilityValue).value;
		}
		
		//se avviene un errore di qualche tipo restituisce il valore minimo possibile di default
		return defaultValue;
	}

}
