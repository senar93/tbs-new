﻿using UnityEngine;
using System.Collections;

// sostituisce il tipo di armatura corrente con quello passato dalla SpecificAbility che la richiama
public class NewArmorType_LinkedEffect : AbsAbilityLinkedEffect
{

	public override object Effect(object target = null,
								  AbilitySender sender = null,
								  AbsEventTagType eventTag = null,
								  AbsAbilityValue baseAbilityValue = null,
								  AbsAbilityValue specificAbilityValue = null,
								  AbsAbilityValue[] battleAbilityValue = null,
								  string optionalParameter = "")
	{

		//controlla che l'abilità sia stata richiamata da uno specific ability value
		if (specificAbilityValue != null && specificAbilityValue is ArmorType_AbilityValue) {
			if (target is StatCalcTarget && (target as StatCalcTarget).targetBase is Armor)
				((target as StatCalcTarget).targetBase as Armor).armorType = (specificAbilityValue as ArmorType_AbilityValue).armorType;

		}

		return 0f;
	}




}