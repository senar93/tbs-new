﻿using UnityEngine;
using System.Collections;
using System;


// implementazione di AbsAbility a cui ogni abilità fa riferimento
// contiene: - parametri comuni a tutte le abilità che fanno riferimento alla stessa BaseAbility
//			 - il un puntatore a una classe di tipo AbsAbilityLinkedEffect, che contiene il codice delle varie abilità
public class BaseAbility : AbsAbility {

	[SerializeField] private EnumApplicationTagType applicationTag;

	public AbsAbilityLinkedEffect linkedEffect;						//link all effetto dell abilità
	public AbsAbilityValue linkedBaseValue;                         //link ai valori di base dell abilità
	[SerializeField] protected bool isPassive;                          //indica se l'abilità è passiva o meno


	#region OVERRIDE
	public override float GetPriority(string optionalParameter = "") {
		return priority;
	}

	public override EnumApplicationTagType GetApplicationTag(string optionalParameter = "") {
		return applicationTag;
	}

	public override object Effect(object target = null,
								  AbilitySender sender = null,
								  AbsEventTagType eventTag = null,
								  AbsAbilityValue baseAbilityValue = null,		//ignorato
								  AbsAbilityValue specificAbilityValue = null,
								  AbsAbilityValue[] battleAbilityValue = null,
								  string optionalParameter = "")
	{
		if (linkedEffect != null)
		{
			// sovrascrittura battleAbilityValue
			baseAbilityValue = linkedBaseValue;

			return linkedEffect.Effect(target, sender, eventTag, baseAbilityValue, specificAbilityValue, battleAbilityValue, optionalParameter);
		}
		else
			return null;
	}

	public override AbsEventTagType GetEventTagType(object target = null,
													AbilitySender sender = null,
													AbsAbilityValue baseAbilityValue = null,
													AbsAbilityValue specificAbilityValue = null,
													AbsAbilityValue[] battleAbilityValue = null,
													string optionalParameter = "") {
		if (linkedEffect != null)
		{
			// sovrascrittura battleAbilityValue
			baseAbilityValue = linkedBaseValue;

			return linkedEffect.GetEventTagType(target, sender, baseAbilityValue, specificAbilityValue, battleAbilityValue, optionalParameter);
		}
		else
			return null;
	}

	public override AbsAbility GetFinalAbility() {
		return this;
	}

	public override bool IsPassive(){
		return isPassive;
	}
	#endregion




}