﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BattleAbilityList {
	public List<AbsAbility> list;


	//costruttore
	public BattleAbilityList() {
		list = new List<AbsAbility>();
	}
	public BattleAbilityList(List<AbsAbility> newList) {
		list = newList;
	} //overload
	public BattleAbilityList(BattleAbilityList newList) {
		list = newList.list;
	} //overload

	//ordina la lista in base alla priorità (ordine decrescente)
	//attualmente implementa un algoritmo di insertion sort
	public BattleAbilityList OrderByPriority() {
		AbsAbility tmp;
		int j;
		float tmpPriority;

		for(int i = 1; i < list.Count; i++) {
			tmp = list[i];
			tmpPriority = tmp.GetPriority();
			j = i - 1;
			while(j >= 0 && list[j].GetPriority() > tmpPriority) {
				list[j + 1] = list[j];
				j--;
			}
			list[j + 1] = tmp;
		}

		return this;
	}

	#region FIND
	//crea un nuovo oggetto BattleAbilityList contenente solo le abilità con il/i tag specificati
	public BattleAbilityList GetNewByApplicationTag(EnumApplicationTagType tag) {
		BattleAbilityList tmp = new BattleAbilityList();
		if (list != null)
			for (int i = 0; i < list.Count; i++)
				if (list[i] != null && list[i].GetApplicationTag() == tag)
					tmp.list.Add(list[i]);

		return tmp;
	}
	public BattleAbilityList GetNewByApplicationTag(params EnumApplicationTagType[] tags) {
		BattleAbilityList tmp = new BattleAbilityList();
		bool goOn = true;
		if (list != null && tags != null)
			for (int i = 0; i < list.Count; i++)
				if (list[i] != null) {
					for (int j = 0; j < tags.Length && goOn; j++)
						if (list[i].GetApplicationTag() == tags[j]) {
							tmp.list.Add(list[i]);
							goOn = false;
						}
					goOn = true;
				}
		
		return tmp;
	} //overload per cercare più tag contemporaneamente
	//crea un nuovo oggetto BattleAbilityList contenente solo le abilità passive / non passive
	public BattleAbilityList GetNewByIsPassive(bool passive = false) {
		BattleAbilityList tmp = new BattleAbilityList();
		if (list != null)
			for (int i = 0; i < list.Count; i++)
				if (list[i] != null && list[i].IsPassive() == passive)
					tmp.list.Add(list[i]);

		return tmp;
	}

	//agiscono in maniera analoga alle funzioni scritte sopra, ma restituiscono una lista di interi, che corrispondono alla posizione dell abilità nella lista originale
	public List<int> GetIndexByApplicationTag(EnumApplicationTagType tag) {
		List<int> tmp = new List<int>();
		if (list != null)
			for (int i = 0; i < list.Count; i++)
				if (list[i] != null && list[i].GetApplicationTag() == tag)
					tmp.Add(i);

		return tmp;
	}
	public List<int> GetIndexByApplicationTag(params EnumApplicationTagType[] tags) {
		List<int> tmp = new List<int>();
		bool goOn = true;
		if (list != null && tags != null)
			for (int i = 0; i < list.Count; i++)
				if (list[i] != null)
				{
					for (int j = 0; j < tags.Length && goOn; j++)
						if (list[i].GetApplicationTag() == tags[j])
						{
							tmp.Add(i);
							goOn = false;
						}
					goOn = true;
				}

		return tmp;
	}
	public List<int> GetIndexByIsPassive(bool passive = false) {
		List<int> tmp = new List<int>();
		if (list != null)
			for (int i = 0; i < list.Count; i++)
				if (list[i] != null && list[i].IsPassive() == passive)
					tmp.Add(i);

		return tmp;
	}
	
	#endregion


}
