﻿using UnityEngine;
using System.Collections;

//classe che contiene il tipo di evento che provoca il calcolo dell abilità
//alcune abilità potrebbero dover essere calcolate solo a seguito di certi eventi
//l'abilità stessa deve occuparsi di controllare se deve essere applicata nell evento che inizia il calcolo

//le implementazioni di questa classe devono permettere di comporre un qualsiasi evento del gioco
//in generale le classi che forniscono l'implementazione di EventTagType devono fornire tutte le informazioni per identificare la situazione in cui sta venendo eseguita l'abilità
//   che non siano gia in qualche modo contenute negli altri parametri della funzione Effect

//   per esempio subire danni da una palla di fuoco lanciata da un unità nemica dovrebbe contenere informazioni del tipo:
//   reciveDamageBySpell, damageType = fire, reciveDamageAoe
//   questo significa che:
//    1: hai ricevuto danni (non deve essere necessario specificarlo manualmente in un caso come questo, deve aggiungere il tag reciveDamage da sola)
//    2: che i danni che hai subito sono di tipo fuoco (avrebbero potuto essere di un altro tipo, o persino di più tipi)
//    3: che il danno è stato subito era ad area (potresti essere tu il bersaglio o meno,
//       un esplosione di fuoco per esempio fa solo danno ad area, una freccia solo danno a target, un fulmine potrebbe fare danno a target sul bersaglio e ad area nelle caselle intorno)
//    4: non viene specificato per esempio che l'unità che lancia la palla di fuoco è un unità nemica, poichè è un informazione che l'abilità può ricavare dal parametro "sender" della funzione Effect
[System.Serializable]
public abstract class AbsEventTagType {

}