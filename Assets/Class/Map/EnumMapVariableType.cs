﻿public enum EnumMapVariableType { 
	// utilizzo camel-case per chiarezza dato che fanno riferimento a delle classi esistenti
	MapNode = 1,
	MapCoord = 2,
	axialCoord = 3,

	undefined = 0
}
