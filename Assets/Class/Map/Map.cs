﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Importante: La mappa dal 20 Giugno 2016 viene gestita con coordinate X, Y e NON più Y, X
[System.Serializable]
public class Map {
//tutte le funzioni che ricevono delle coordinate devono prevedere l'overload per funzionare sia con un oggetto MapCoord, sia con 2 interi x,t
	public static int MAX_COORDS_VALUE = 2048; // valore dell'ultima coordinata è MAX_COORDS_VALUE - 1, quindi es. 2047 anziché 2048. Questo numero indica il numero di coordinate esistenti.

	public List<List<MapNode>> map;
	[SerializeField] private int maxX, maxY = MAX_COORDS_VALUE;

	public int[,,] directions = new int[2, 6, 2] {
		{ {+1,  0}, {0, -1}, {-1, -1},
			{-1,  0}, {-1, +1}, {0, +1} },
		{ {+1,  0}, {+1, -1}, {0, -1},
			{-1,  0}, {0, +1}, {+1, +1} }
	};
	

	//DA IMPLEMENTARE (cubic coord)
	//restituisce true se le coordinate passate come parametro esistono all interno della mappa (non ne superano i limiti)
	public bool ValidCoord(int x, int y) {
		if(x < maxX && x >= 0 && y < maxY && y >= 0)
			return true;
		return false;
	}
	public bool ValidCoord(MapCoord coord) {
		if (coord != null)
			return ValidCoord(coord.x, coord.y);
		else
			return false;
	} //overload
	public bool ValidCoord(MapNode coord) {
		if (coord != null)
			return ValidCoord(coord.GetCoord().x, coord.GetCoord().y);
		else
			return false;
	} //overload

	//DA DECIDERE COME E SE IMPLEMENTARE
	//questa funzione sostituisce il nodo target con il nuovo nodo passato come parametro
	//public void ReplaceNode(AbsMapNode target, AbsMapNode newNode) { }

	//DA IMPLEMENTARE
	//restituisce una nuova mappa formata da un sottoinsieme dei nodi della mappa che richiama la funzione
	public Map Select(EnumMapSelectNodeType mode, params object[] parameters) {
		switch(mode) {
			case EnumMapSelectNodeType.all:
				return this;
			case EnumMapSelectNodeType.row:
				return SelectRow(parameters);
//			case EnumMapSelectNodeType.pathfinding:
//				break;
//			case EnumMapSelectNodeType.list:
//				break;
			
			default:
				return new Map();
		}
	}

	// [0] = direzione, [1] = raggio, [2(3)] = nodo di partenza
	private Map SelectRow(params object[] parameters) {
		Debug.Log("SelectRow START");
		EnumMapVariableType mapVariableType = ValidateRowParameters(parameters);
		EnumMapDirectionType direction = EnumMapDirectionType.right;
		int radius = 0;
		int coordX = -1, coordY = -1;

		if (mapVariableType != EnumMapVariableType.undefined && parameters[1] is int && (int)parameters[1] > 0) {

			direction = (EnumMapDirectionType)parameters[0];
			radius = (int)parameters[1];
			
			if (mapVariableType == EnumMapVariableType.MapCoord) {
				coordX = (parameters[2] as MapCoord).x;
				coordY = (parameters[2] as MapCoord).y;
			} else if (mapVariableType == EnumMapVariableType.MapNode) {
				coordX = (parameters[2] as MapNode).GetCoord().x;
				coordY = (parameters[2] as MapNode).GetCoord().y;
			} else if (mapVariableType == EnumMapVariableType.axialCoord) {
				coordX = (int)parameters[2];
				coordY = (int)parameters[3];
			}

		}

		List<int[]> tmpResult = new List<int[]>();
		Map result = new Map();

		if (ValidCoord(coordX, coordY)) {
			
			tmpResult.Add(new int[2] { coordX, coordY });

			int parity, sumX, sumY;

			for (int i = 0; i < radius-1; i++) {
				parity = coordY % 2; // se siamo su una riga pari o no

				sumX = directions[parity, (int)direction, 0];
				sumY = directions[parity, (int)direction, 1];

				coordX += sumX;
				coordY += sumY;
			
				if (ValidCoord(coordX, coordY))
					tmpResult.Add(new int[2] { coordX, coordY });
			}


			if (tmpResult.Count > 0) {
				int firstCoordX = tmpResult[0][0];
				int firstCoordY = tmpResult[0][1];
				int lastCoordX = tmpResult[tmpResult.Count-1][0];
				int lastCoordY = tmpResult[tmpResult.Count-1][1];

				int shiftX = Mathf.Min(firstCoordX, lastCoordX);
				int shiftY = Mathf.Min(firstCoordY, lastCoordY);

				result = new Map(Mathf.Abs(lastCoordX - firstCoordX) + 1, Mathf.Abs(lastCoordY - firstCoordY) + 1 );

				for (int i = 0; i < tmpResult.Count; i++) {
					int tmpX = tmpResult[i][0];
					int tmpY = tmpResult[i][1];

					result.map[tmpX-shiftX][tmpY-shiftY] = map[tmpX][tmpY];
				}

				//DA RIVEDERE
				// da un errore nei nodi selezionati
				//disegnando una riga a dalla casella 24,24, andando a destra di 5
				//  Select(EnumMapSelectNodeType.row, EnumMapDirectionType.right, 5, 24, 24);
				//    il valore restituito dovrebbe essere una lista contenente le caselle:
				//		{24,24}, {25,24}, {26,24}, {27,24}, {28,24}
				//    i risultati reali invece sono
				//		{24,24}, {23,24}, {22,25}, {21,25}, {20,26}, {19,26}

				//    quindi viene contata una casella di troppo
				//	  dando raggio 1 viene restituito il nodo di partenza E un altro nodo {23,24} al posto del solo nodo di partenza

				//	  i test puoi farli dal file MapScript.cs, li nella funzione Start viene richiamata Select
				for (int i = 0; i < tmpResult.Count; i++)
					Debug.Log(tmpResult[i][0] + " , " + tmpResult[i][1]);
				//queste 2 istruzioni provacano un errore ArgumentOutOfRangeException
				//il ciclo è anche sbagliato a livello logico poichè assegna ad ogni elemento della mappa (presumibilmente con le coordinate giuste)
				//  un nodo con delle coordinate fisse, quindi la mappa avrà alla fine dei nodi con tutte le coordinate uguali dentro di se (anche se saranno nella posizione giusta
				/*
				foreach (var element in tmpResult) {
					Debug.Log(element[1] + " , " + element[0]);
					result.map[element[1]][element[0]] = new MapNode(coordX, coordY, this);
				}
				*/

			}

		}
		Debug.Log("END");
		return result;
	}




	// [0] = direzione, [1] = raggio, [2(3)] = nodo di partenza
	private EnumMapVariableType ValidateRowParameters(params object[] parameters) {

		if (parameters.Length == 3 || parameters.Length == 4) {
			if (parameters[0] is EnumMapDirectionType && parameters[1] is int) {
				
				if (parameters.Length == 3) {
					if (parameters[2] is MapNode)
						return EnumMapVariableType.MapNode;
					else if (parameters[2] is MapCoord)
						return EnumMapVariableType.MapCoord;
					
				} else if (parameters.Length == 4) {
					if (parameters[2] is int && parameters[3] is int)
						return EnumMapVariableType.axialCoord;
				}

			}
		}
			
		return EnumMapVariableType.undefined;
	}


	#region CHANGE PARAMETER
	//imposta il valore di X secondo la modalità scelta
	public void ChangeMaxX(int newX, EnumChangeModeType mode = EnumChangeModeType.set) {
		switch(mode) {
			case EnumChangeModeType.set:
				if (newX >= 0)
					if (newX < MAX_COORDS_VALUE)
						maxX = newX;
					else
						maxX = MAX_COORDS_VALUE - 1;
				else
					maxX = 0;
				break;
			case EnumChangeModeType.incremental:
				if (maxX + newX >= 0)
					if(maxX + newX < MAX_COORDS_VALUE)
						maxX += newX;
					else
						maxX = MAX_COORDS_VALUE - 1;
				else
					maxX = 0;
				break;
		}
	}
	//imposta il valore di Y secondo la modalità scelta
	public void ChangeMaxY(int newY, EnumChangeModeType mode = EnumChangeModeType.set) {
		switch (mode) {
			case EnumChangeModeType.set:
				if (newY >= 0)
					if (newY < MAX_COORDS_VALUE)
						maxY = newY;
					else
						maxY = MAX_COORDS_VALUE - 1;
				else
					maxY = 0;
				break;
			case EnumChangeModeType.incremental:
				if (maxY + newY >= 0)
					if(maxY + newY < MAX_COORDS_VALUE)
						maxY += newY;
					else
						maxY = MAX_COORDS_VALUE - 1;
				else
					maxY = 0;
				break;
		}
	}

	#endregion


	#region GET
	public int GetMaxX() {
		return maxX;
	}
	public int GetMaxY() {
		return maxY;
	}
	//restituisce tutti i nodi della mappa come se fossero un unica lista
	public List<MapNode> GetList()
	{
		List<MapNode> tmpList = new List<MapNode>();
		if (map != null)
			for (int i = 0; i < map.Count; i++)
				for (int j = 0; j < map[i].Count; j++)
					tmpList.Add(map[i][j]);

		return tmpList;
	}

	#endregion

	// la mappa viene inizializzata con coordinate X, Y (in questo ordine)
	#region CONSTRUCTOR
	public Map() {	}
	public Map(int x, int y) {
		ChangeMaxX(x);
		ChangeMaxY(y);
		map = new List<List<MapNode>>();
		for(int i = 0; i < maxX; i++) {
			map.Add(new List<MapNode>());
			for(int j = 0; j < maxY; j++) 
				map[i].Add(null);
		}

	}
	#endregion

}
