﻿// in base al sistema odd-r http://www.redblobgames.com/grids/hexagons/#coordinates
public enum EnumMapDirectionType { 

    right = 0,
    topRight = 1,
    topLeft = 2,
    left = 3,
    botLeft = 4,
    botRight = 5
}
