﻿public class ObjectPosition {

	public bool outsideOfTheMap;
	private MapNode node;
	private MapCoord coord;

	

	//DA IMPLEMENTARE
	//setta le coordinate del oggetto, se setNodeByCoord = true inizializza anche il puntatore al nodo
	public void SetCoord(int x, int y, bool setNodeByCoord = true) {
		outsideOfTheMap = false;
		coord = new MapCoord(x, y);
		if (setNodeByCoord)
			; //richiama la mappa per trovare il nodo di coordinate coord e lo assegna a node
	}
	public void SetCoord(MapCoord newCoord, bool setNodeByCoord = true) {
		SetCoord(newCoord.x, newCoord.y, setNodeByCoord);
	} //overload




	#region CHANGE PARAMETER
	//inizializza il puntatore al nodo, se setCoordByNode = true inizializza anche le coordinate
	public void SetNode(MapNode newNode, bool setCoordByNode = true) {
		outsideOfTheMap = false;
		node = newNode;
		if (setCoordByNode && node != null)
			SetCoord(node.GetCoord(), false);
	}

	#endregion


	#region GET
	//viene restituito il riferimento al nodo NON un nuovo oggetto copia del nodo
	public MapNode GetNode() {
		return node;
	}
	//viene restituita una copia delle coordinate
	public MapCoord GetCoord() {
		return coord;
	}	
	public int GetCoordX() {
		return coord.x;
	}
	public int GetCoordY() {
		return coord.y;
	}

	#endregion

}
