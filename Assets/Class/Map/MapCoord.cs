﻿using UnityEngine;
using System.Collections;

public class MapCoord {

	public int x, y;

	#region CONSTRUCTOR
	public MapCoord(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public MapCoord(MapCoord tmp) {
		this.x = tmp.x;
		this.y = tmp.y;
	} //overload
	//public MapCoord() { }

	#endregion

	//ridefinizione del cast per MapCoord
	//ora è possibile assegnare un valore di tipo ObjectPosition a uno di tipo MapCoord
	public static implicit operator MapCoord(ObjectPosition tmp) {
		return new MapCoord(tmp.GetCoord());
	}


}
