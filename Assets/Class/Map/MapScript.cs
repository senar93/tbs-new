﻿using UnityEngine;
using System.Collections;

public class MapScript : MonoBehaviour {

	public Map map;

	//TEST
	public void Start() {
		map = new Map(50, 50);
		/*Debug.Log(map.GetList());
		for (int i = 0; i < map.map.Count; i++)
			for (int j = 0; j < map.map[i].Count; j++)
				Debug.Log(i + " , " + j + " = " + map.map[i][j]);*/

		for (int x = 0; x < map.GetMaxX(); x++) {
			for (int y = 0; y < map.GetMaxY(); y++) {
				map.map[x][y] = new MapNode(x, y, map);
			}
		}

		Map tmp = map.Select(EnumMapSelectNodeType.row, EnumMapDirectionType.topRight, 5, 2, 4);

		for (int i = 0; i < tmp.map.Count; i++) {
			for (int j = 0; j < tmp.map[0].Count; j++) {
				if (tmp.map[i][j] != null)
					Debug.Log("X: " + i + " Y: " + j);
			}
		}
	}

}
