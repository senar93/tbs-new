﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapNode {

	protected MapCoord coordinate;
	protected Map map;
	
	public EnumTerrainType terrainType;
	public int height;
	//è necessario implementare un sistema per salvare i nodi collegati in modi particolari e i particolari costi (e requisiti) per passare tra nodi collegati in questo modo
	//public List<ObjectPosition> linkedNode;			//indica eventuali altri nodi adiacenti in maniera "speciale" a questo (es. portale tra un nodo e l'altro)

	//DA IMPLEMENTARE
	//lista di puntatori all unità presenti sul nodo, dal nodo deve essere possibile accedere alla lista delle unità che si trovano su di esso
	//non è stata dichiarata ancora la lista poichè non esiste ancora la classe BattleGroup

	//DA IMPLEMENTARE
	//eventuali altri parametri del nodo

	#region CONSTRUCTOR
	public MapNode(int x, int y, Map newMap) {
		this.map = newMap;
		coordinate = new MapCoord(x, y);
	}
	public MapNode(MapCoord tmpCoord, Map newMap) {
		this.map = newMap;
		coordinate = new MapCoord(tmpCoord.x, tmpCoord.y);
	} //overload
	
	#endregion


	#region CHANGE ATTRIBUTE
	//sostituisce le coordinate del nodo con delle nuove coordinate
	public void ChangeCoordinate(MapCoord newCoord) {
		coordinate = new MapCoord(newCoord);
	}
	public void ChangeCoordinate(int x, int y) {
		coordinate = new MapCoord(x, y);
	} //overload

	#endregion



	#region GET
	//restituisce le coordinate del nodo
	public MapCoord GetCoord() {
		return new MapCoord(coordinate);
	}
	public Map GetMap() {
		return map;
	}

	#endregion

}
