﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class NetworkPlayer : NetworkBehaviour {

	public bool newMsg = false;
	public List<string> msg;



	#region CMD (Client to Server)
	[Command] public void Cmd_SendMsg(string msg) {
		Debug.Log("Recive msg from Client : " + msg);
		newMsg = true;
		this.msg.Add(msg);
	}
	#endregion



	#region RPC (Server to Client)
	[ClientRpc] public void Rpc_SendMsg(string msg) {
		Debug.Log("Recive msg from Server : " + msg);
		newMsg = true;
		this.msg.Add(msg);
	}

	[ClientRpc] public void Rpc_RenamePlayer(string msg) {
		Debug.Log("Server set Player.name to " + msg);
		this.name = msg;
	}
	#endregion



	//TEST
	public void Start() {
		if (isClient) {
			Cmd_SendMsg("Ciao sono " + this.name);
			Cmd_SendMsg("messaggio di prova CLIENT 1");
			Cmd_SendMsg("messaggio di prova CLIENT 2");
			Cmd_SendMsg("messaggio di prova CLIENT 3");
		}
		if(isServer) {
			Rpc_SendMsg("messaggio di prova SERVER 1");
		}

	}



}
