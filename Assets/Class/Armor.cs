﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Armor {
	public static float MIN_ARMOR_ALOWED = 0;

	public float armorValue;
	public EnumArmorType armorType;

	public Armor() { }
	public Armor(float value, EnumArmorType type) {
		armorValue = value;
		armorType = type;
	}
}
