﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//fornisce tutte le informazioni sulla razza selezionata in base alla deployModeSelezionata 
public class Race : MonoBehaviour {

	[SerializeField] private SpecificDeployModeInfo[] dataByDeployMode;

	//i dati comuni a tutti i possibili HeroDeployMode consentiti dalla razza sono inseriti in queste variabili
	//ogni elemento di dataByDeployMode poi specifica eventuali informazioni ulteriori
	[SerializeField] private Item[] itemAllowed;
	[SerializeField] private UnityBase[] unityAllowed;
	[SerializeField] private AuxiliaryBase[] heroAuxiliaryList;
	[SerializeField] private AbsAbility[] freeAbility;                //se l'eroe deve avere qualche abilità che dipende solo dalla razza ed è indipendente da tutti gli altri talenti va inserita qui



	//restituisce la lista di HeroDeployMode implementate per la razza
	public List<EnumHeroDeployMode> GetHeroDeployModeAllowed() {
		List<EnumHeroDeployMode> tmp = new List<EnumHeroDeployMode>();
		if (dataByDeployMode != null)
			for (int i = 0; i < dataByDeployMode.Length; i++)
				if (dataByDeployMode[i] != null)
					tmp.Add(dataByDeployMode[i].deployMode);

		return tmp;
	}
	//restituisce l'indice del array in cui si trova l'elemento selezionato, -1 altrimenti
	public int FindByDeployMode(EnumHeroDeployMode selection) {
		if (dataByDeployMode != null)
			for (int i = 0; i < dataByDeployMode.Length; i++)
				if (dataByDeployMode[i] != null && dataByDeployMode[i].deployMode == selection)
					return i;

		return -1;
	}

	//funzioni che restituiscono i parametri sia di Race che del HeroInfo del indice selezionato
	public Item[] GetItemAllowed(int index) {
		if (index >= 0 && index < dataByDeployMode.Length) {
			Item[] tmp = new Item[ itemAllowed.Length + dataByDeployMode[index].itemAllowed.Length];
			itemAllowed.CopyTo(tmp, 0);
			dataByDeployMode[index].itemAllowed.CopyTo(tmp, itemAllowed.Length);
			return tmp;
		}
		return new Item[0];
	}
	public Item[] GetItemAllowed(EnumHeroDeployMode selection) {
		return GetItemAllowed( FindByDeployMode(selection) );
	} //overload
	public UnityBase[] GetUnityAllowed(int index)
	{
		if (index >= 0 && index < dataByDeployMode.Length) {
			UnityBase[] tmp = new UnityBase[unityAllowed.Length + dataByDeployMode[index].unityAllowed.Length];
			unityAllowed.CopyTo(tmp, 0);
			dataByDeployMode[index].unityAllowed.CopyTo(tmp, unityAllowed.Length);
			return tmp;
		}
		return new UnityBase[0];
	}
	public UnityBase[] GetUnityAllowed(EnumHeroDeployMode selection) {
		return GetUnityAllowed( FindByDeployMode(selection) );
	} //overload
	public AuxiliaryBase[] GetHeroAuxiliaryAllowed(int index)
	{
		if (index >= 0 && index < dataByDeployMode.Length) {
			AuxiliaryBase[] tmp = new AuxiliaryBase[heroAuxiliaryList.Length + dataByDeployMode[index].heroAuxiliaryList.Length];
			heroAuxiliaryList.CopyTo(tmp, 0);
			dataByDeployMode[index].heroAuxiliaryList.CopyTo(tmp, heroAuxiliaryList.Length);
			return tmp;
		}
		return new AuxiliaryBase[0];
	}
	public AuxiliaryBase[] GetHeroAuxiliaryAllowed(EnumHeroDeployMode selection) {
		return GetHeroAuxiliaryAllowed( FindByDeployMode(selection) );
	} //overload
	public AbsAbility[] GetFreeAbilityAllowed(int index) {
		if (index >= 0 && index < dataByDeployMode.Length)
		{
			AbsAbility[] tmp = new AbsAbility[freeAbility.Length + dataByDeployMode[index].freeAbility.Length];
			freeAbility.CopyTo(tmp, 0);
			dataByDeployMode[index].freeAbility.CopyTo(tmp, freeAbility.Length);
			return tmp;
		}
		return new AbsAbility[0];
	}
	public AbsAbility[] GetFreeAbilityAllowed(EnumHeroDeployMode selection) {
		return GetFreeAbilityAllowed( FindByDeployMode(selection) );
	} //overload
	public int GetHeroAuxiliaryLimit(int index) {
		if (index >= 0 && index < dataByDeployMode.Length)
			return dataByDeployMode[index].heroAuxiliaryLimit;
		return 0;
	}
	public int GetHeroAuxiliaryLimit(EnumHeroDeployMode selection) {
		return GetHeroAuxiliaryLimit( FindByDeployMode(selection) );
	} //overload
	public Talent[] GetTalentRoots(int index) {
		if (index >= 0 && index < dataByDeployMode.Length)
			return dataByDeployMode[index].talentRoots;
		return new Talent[0];
	}
	public Talent[] GetTalentRoots(EnumHeroDeployMode selection) {
		return GetTalentRoots( FindByDeployMode(selection) );
	} //overload
	public EnumHeroDeployMode GetHeroDeployModeByIndex(int index) {
		if (index >= 0 && index < dataByDeployMode.Length)
			return dataByDeployMode[index].deployMode;
		return EnumHeroDeployMode.none;
	}



}
