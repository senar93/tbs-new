﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {
	public EnumItemType itemType;
	public AbsObjectRequisite requisite;
	public AbsAbility[] abilityGive;

}