﻿using UnityEngine;
using System.Collections;

//fornisce le informazioni specifiche di una deployMode possibile per la razza selezionata
public class SpecificDeployModeInfo : MonoBehaviour {

	public EnumHeroDeployMode deployMode;

	public int heroAuxiliaryLimit;
	public AuxiliaryBase[] heroAuxiliaryList;
	public Talent[] talentRoots;
	//da aggiungere informazioni su eventuali limitazioni nel numero di oggetti di un certo tipo
	public Item[] itemAllowed;
	public UnityBase[] unityAllowed;

	public AbsAbility[] freeAbility;                //se l'eroe deve avere qualche abilità che dipende solo dalla razza ed è indipendente da tutti gli altri talenti va inserita qui




}
