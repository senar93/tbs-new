﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class BattleArmy : MonoBehaviour {
	//da implementare: puntatore al giocatore che sta controllando l'armata
	public Army source;
	public List<BattleGroup> groupList;

	//abilità proprie dell armata, normalmente fornite dall eroe
	//in questa lista si trovano abilità particolari, normalmente il loro effetto inizia e finisce prima dell inizio della battaglia vera e propria
	//es. un abilità che modifica l'area di schieramento delle truppe fornita dall eroe diventerebbe un abilità del armata e influenza la fase di schieramento delle unità
	//es. eventuali rinforzi potrebbero essere considerati un abiltà del armata attivabile in determinati momenti
	public BattleAbilityList abilityList;


	//funzione per creare i gruppi in base anche al heroDeployMode selezionato e alla conseguente implementazione di AbsHero
	public void Create(Army tmpSource) {
		source = tmpSource;

		//quando il gruppo indicato dal elemento della lista viene creato l'elemento viene rimosso dalla lista, la lista viene SEMPRE percorsa al contrario
		List<int> indexToProcess = new List<int>();
		for (int i = 0; i < source.unityList.Count; i++)
			indexToProcess.Add(i);

		//setta tutto quello che riguarda l'eroe, e qualsiasi unità gruppo ad esso collegato
		if(source.hero != null)
			switch(source.hero.heroDeployMode) {
				case EnumHeroDeployMode.general:
					break;
				case EnumHeroDeployMode.linkedToUnity:
					break;

				default:
					break;
			}


		//setta i restanti gruppi indicati dalla lista indexToProcess
		for(int i = indexToProcess.Count-1; i >= 0; i--) {
			//DA SISTEMARE
			//al momento il nome dell oggetto è il numero del indice del ciclo for in cui viene creato

			//istanzia il battlegroup prendendo i valori da  source.unityList[indexToProcess[i]]
			GameObject tmp = BattleArmy.InstantiateStandardGroup(source.unityList[indexToProcess[i]],
																 this.gameObject,
																 i.ToString() );
			//aggiunge l'oggetto alla lista dei gruppi e lo rimuove dalla lista degli indici da processare
			groupList.Add( tmp.GetComponent<BattleGroup>() );
			indexToProcess.Remove(i);
		}

		InstantiateBattleResource();

	}

	//istanzia un nuovo battlegroup
	public static GameObject InstantiateStandardGroup(UnityArmy obj, GameObject creator, string name = "undefined") {
		GameObject tmpGameObject = new GameObject();
		tmpGameObject.transform.parent = creator.transform;
		tmpGameObject.name = name;

		BattleGroup tmpBattleGroup = tmpGameObject.AddComponent<BattleGroup>();

		tmpBattleGroup.unityNumber = obj.number;
		tmpBattleGroup.position = new ObjectPosition();
		tmpBattleGroup.source = obj;
		tmpBattleGroup.ability = new BattleAbilityList();
		tmpBattleGroup.ability.list = obj.GetAllAbility();

		tmpBattleGroup.isAlive = true;
		tmpBattleGroup.hasHealth = true;

		tmpBattleGroup.isHide = false;
		tmpBattleGroup.isBaseGroup = true;
		tmpBattleGroup.isPlaying = true;
		tmpBattleGroup.canBeDamaged = true;
		tmpBattleGroup.canBeKilled = true;
		tmpBattleGroup.canBeDestroyed = true;
		tmpBattleGroup.canBeTargeted = true;

		return tmpGameObject;
	}

	//per ogni gruppo crea e associa una BattleAbility che conterrà la risorsa attuale specificata del unità, e la imposta al valore corretto
	public void InstantiateBattleResource() {
		for(int i = 0; i < groupList.Count; i++) {
			//il valore iniziale è il valore in hp restituito da BattleAbility
			groupList[i].ActStatAllocation(EnumApplicationTagType.healthActBase, "Health Act (battle)");
			
		}
	}

}
