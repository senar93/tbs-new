﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleGroup : MonoBehaviour {
	public static float MIN_HEALTH_ALOWED = 0.00001f;

	//puntatore (o un qualche tipo di riferimento) al giocatore che posside il gruppo
	//puntatore (o un qualche tipo di riferimento) al giocatore che controlla il gruppo
	public UnityArmy source;
	public int unityNumber;
	public BattleAbilityList ability;
	public ObjectPosition position;

	public List<EnumMapDirectionType> direction = new List<EnumMapDirectionType>();		//lista di direzioni a cui è rivolto il gruppo, se è vuoto il gruppo non è rivolto in alcuna direzione (es, un sasso)
	public bool isAlive;
	public bool hasHealth;

	// sono parametri assoluti
	// es: se un oggetto ha canBeTargeted = false, nessun giocatore lo potrà mai selezionare in nessun modo
	//   abilità del tipo: rendi questo gruppo non selezionabile dalle magie di tutti (o da alcuni) giocatori per X turni vengono implementate tramite abilità e NON variano questi parametri
	// alcune abilità potrebbero variare temporaneamente o permanentemente il valore di questi parametri, bisogna però assicurarsi che l'effetto sia davvero quello voluto (per esempio se si mette isHide a true, nessuno, nemmeno il proprietario dell unità la vedrà)
	// sono tendenzialmente usati in gruppi particolari, quali potrebbero essere un effetto climatico sulla mappa (es: una tempesta di neve su alcune caselle)
	// o per creare oggetti dal funzionamento particolare, diversi dai normali gruppi che formano l'esercito del giocatore
	public bool isPlaying;								//identifica se l'oggetto è inserito nel calcolo dei turni o no
	public bool canBeTargeted;
	public bool isHide;
	public bool canBeDamaged;							//un oggetto che può essere danneggiato e possiede degli hp può morire
	public bool canBeKilled;							//un oggetto che può essere ucciso quando viene ucciso rimane sul terreno morto (ma esiste ancora come oggetto)
	public bool canBeDestroyed;							//un oggetto che può essere distrutto quando viene distrutto (che non vuol dire ucciso) viene completamente rimosso dal gioco
	public bool isBaseGroup;							//i gruppi scherati all inizio della battaglia sono i gruppi base, eventuali unità evocate ecc no (possono però esserci delle eccezioni), quando i gruppi base di un giocatore sono tutti morti quel giocatore perde la partita


	#region TOTAL STAT CALC
				//per ora è pubblica solo per i test
	public float HealthMaxCalc(AbilitySender sender) {
		float tmpValue = (float) StatCalcBase(EnumApplicationTagType.healthMaxBase, sender);
		tmpValue = StatCalcInc(EnumApplicationTagType.healthMaxCalcStart,tmpValue, 0, sender);
		tmpValue = StatCalcInc(EnumApplicationTagType.healthMaxCalcInc, tmpValue, 0, sender);
		tmpValue = StatCalcIncXC(EnumApplicationTagType.healthMaxCalcIncXC, tmpValue, 0, sender);
		tmpValue = StatCalcInc(EnumApplicationTagType.healthMaxCalcFinal, tmpValue, 0, sender);

		if (tmpValue < MIN_HEALTH_ALOWED)
			tmpValue = MIN_HEALTH_ALOWED;

		return tmpValue;
	}

	public Armor ArmorCalc(AbilitySender sender) {
		object tmp = StatCalcBase(EnumApplicationTagType.armorBase, sender);
		Armor tmpValue;
		
		if (tmp is Armor)
			tmpValue = (Armor)tmp;
		else
			tmpValue = new Armor(0,EnumArmorType.none);
		
		StatCalcInc(EnumApplicationTagType.armorAllCalcStart, 0, tmpValue, sender);
		tmpValue.armorValue = StatCalcInc(EnumApplicationTagType.armorValueCalcStart, tmpValue.armorValue, tmpValue, sender);
		tmpValue.armorValue = StatCalcInc(EnumApplicationTagType.armorValueCalcInc, tmpValue.armorValue, tmpValue, sender);
		tmpValue.armorValue = StatCalcIncXC(EnumApplicationTagType.armorValueCalcIncXC, tmpValue.armorValue, tmpValue, sender);
		tmpValue.armorValue = StatCalcInc(EnumApplicationTagType.armorValueCalcFinal, tmpValue.armorValue, tmpValue, sender);
		StatCalcInc(EnumApplicationTagType.armorTypeCalc, 0, tmpValue, sender);
		StatCalcInc(EnumApplicationTagType.armorAllCalcFinal, 0, tmpValue, sender);

		if (tmpValue.armorValue < Armor.MIN_ARMOR_ALOWED)
			tmpValue.armorValue = Armor.MIN_ARMOR_ALOWED;

		return tmpValue;
	}


	#endregion


	#region ACT STAT ALLOCATION
	//controlla se l'oggetto possiede un abilità con tag healthActBase, se la possiede E l'oggetto è di tipo BaseAbility, alloca un nuovo GameObject che sarà una battle ability che contiene la vita attuale dell unità 
	public void ActStatAllocation(EnumApplicationTagType tag, string name = "Generic Act Stat (battle)") {

		List<int> tmpList = ability.GetIndexByApplicationTag(tag);

		if(tmpList.Count > 0)
			if(ability.list[tmpList[0]] is BaseAbility) {
				GameObject tmpGameObject = new GameObject();
				tmpGameObject.transform.parent = this.transform;
				tmpGameObject.name = name;

				AbilitySender tmpSender = new AbilitySender();
				tmpSender.ability = ability.list[tmpList[0]];
				tmpSender.abilityOwner = this;

				BattleAbility tmpBattleAbility = tmpGameObject.AddComponent<BattleAbility>();

				//generalizando queste due istruzioni è possibile scrivere un unica funzione per l'allocazione di qualsiasi risorsa di questo tipo
				//sarà probabilmente necessarrio passare una funzione come parametro
				//la generalizzazione verrà effettuata tramite uno switch, in futuro si indagherà per utilizare sistemi migliori
				//tmpBattleAbility.linkedBattleValue = tmpGameObject.AddComponent<FloatValue_AbilityValue>();	//da generalizare
				//(tmpBattleAbility.linkedBattleValue as FloatValue_AbilityValue).value = HealthMaxCalc(tmpSender); //da generalizare
				Switch_ActStatAllocation(tag, tmpGameObject, tmpBattleAbility, tmpSender);

				tmpBattleAbility.linkedAbility = ability.list[tmpList[0]];
				tmpBattleAbility.SetPreviousBattleAbility(null);
				ability.list[tmpList[0]] = tmpBattleAbility;
			} 

	}

	//per ogni tag gestito nella funzione ActStatAllocation è necessario scrivere un case nello switch dove si imposta linkedBattleValue e i suoi valori
	protected void Switch_ActStatAllocation(EnumApplicationTagType tag,
											GameObject tmpGameObject,
											BattleAbility tmpBattleAbility,
											AbilitySender tmpSender) {
		
		switch(tag) {
			case EnumApplicationTagType.healthActBase:
				tmpBattleAbility.linkedBattleValue = tmpGameObject.AddComponent<FloatValue_AbilityValue>();
				(tmpBattleAbility.linkedBattleValue as FloatValue_AbilityValue).value = HealthMaxCalc(tmpSender);
				break;
			//altri case per ogni battletag gestito in questo modo
		}

	}

	#endregion







	#region GENERIC ABILITY CALC
	//cerca tra le abilità l'abilità con tag specificato con la priorità più alta, e la usa come base per il calcolo, ignora le altre
	private object StatCalcBase(EnumApplicationTagType tag, AbilitySender sender) {
		BattleAbilityList tmpList = ability.GetNewByApplicationTag(tag).OrderByPriority();
		if (tmpList != null && tmpList.list != null && tmpList.list.Count > 0) {
			AbsAbility tmp = tmpList.list[0];
			if (tmp != null)
				return tmp.Effect(this, sender);
		}
		return 0;
	}
	//calcola gli incrementi fissi di una statistica
	//le abilità richiamate da questa funzione restituiscono solo l'incremento della statistica, oppure la variano e restituiscono 0
	//gli incrementi con la stessa priorità sono applicati contemporaneamente
	private float StatCalcInc(EnumApplicationTagType tag, float value, object statBase, AbilitySender sender) {
		//gli incrementi con la stessa priorty sono applicati contemporaneamente
		BattleAbilityList tmpList = ability.GetNewByApplicationTag(tag).OrderByPriority();

		if (tmpList != null && tmpList.list != null) {
			int tmpMaxIndex = tmpList.list.Count;
			float tmpValue = 0;
			float lastPriority = 0;
			
			//se esiste almeno un abilità imposta i valori di lastPriority e tmpValue ed esegue l'abilità
			if (tmpMaxIndex > 0)
				if(tmpList.list[0] != null) {
					lastPriority = tmpList.list[0].GetPriority();
					tmpValue = (float)tmpList.list[0].Effect(new StatCalcTarget(this, value, statBase), sender);
				}
			
			//se c'è più di un abilità le esegue ciclicamente, e aggiorna il contenuto di value solo quando varia priority
			for (int i = 1; i < tmpMaxIndex; i++) {
				if (tmpList.list[i] != null) {
					//se cambia la priorità aggiorna il contenuto di value, resetta tmpValue e aggiorna lastPriority
					if (tmpList.list[i].GetPriority() != lastPriority) {
						value += tmpValue;
						tmpValue = 0;
						lastPriority = tmpList.list[i].GetPriority();
					}
					tmpValue += (float)tmpList.list[i].Effect(new StatCalcTarget(this, value, statBase), sender);
				}
			}

			//aggiorna il contenuto di value con l'ultimo valore impostato dalle abilità con priority più basso
			value += tmpValue; 
		}

		return value;
	}
	//calcola gli incrementi % di una statistica
	//le abilità richiamate da questa funzione restituiscono solo l'incremento della statistica, oppure la variano e restituiscono 0
	//gli incrementi con la stessa priorità sono applicati contemporaneamente
	private float StatCalcIncXC(EnumApplicationTagType tag, float value, object statBase, AbilitySender sender) {
		//gli incrementi con la stessa priority sono applicati contemporaneamente
		BattleAbilityList tmpList = ability.GetNewByApplicationTag(tag).OrderByPriority();

		if (tmpList != null && tmpList.list != null)
		{
			int tmpMaxIndex = tmpList.list.Count;
			float tmpValue = 1;
			float lastPriority = 0;

			//se esiste almeno un abilità imposta i valori di lastPriority e tmpValue ed esegue l'abilità
			if (tmpMaxIndex > 0 && tmpList.list[0] != null) {
				lastPriority = tmpList.list[0].GetPriority();
				tmpValue += (float)tmpList.list[0].Effect(new StatCalcTarget(this, value, statBase), sender);
			}

			//se c'è più di un abilità le esegue ciclicamente, e aggiorna il contenuto di value solo quando varia priority
			for (int i = 1; i < tmpMaxIndex; i++) {
				if (tmpList.list[i] != null) {
					//se cambia la priorità aggiorna il contenuto di value, resetta tmpValue e aggiorna lastPriority
					if (tmpList.list[i].GetPriority() != lastPriority) {
						value *= tmpValue;
						tmpValue = 1;
						lastPriority = tmpList.list[i].GetPriority();
					}
					tmpValue += (float)tmpList.list[i].Effect(new StatCalcTarget(this, value, statBase), sender);
				}
			}

			//aggiorna il contenuto di value con l'ultimo valore impostato dalle abilità con priority più basso
			value *= tmpValue;
		}

		return value;
	}

	#endregion


}
