﻿using UnityEngine;
using System.Collections;

public class StatCalcTarget {
	public object target;					//oggetto che possiede la statistica in questione
	public object targetValue;				//la statistica numerica che viene modificata nel calcolo dell abilità
	public object targetBase;				//la statistica totale (numerica + eventuali altri parametri o altri valori numerici)
											//viene aggiornata al termine del calcolo (o durante ma deve farlo l'abilità stessa)

	public StatCalcTarget(object target, object targetValue, object targetBase) {
		this.target = target;
		this.targetValue = targetValue;
		this.targetBase = targetBase;
	}
}
