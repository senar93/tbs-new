﻿using UnityEngine;
using System.Collections;

public class TEST : MonoBehaviour {


	public bool testAbilityFlag = true;
	public AbsAbility testAb;
	private void TEST_Ability() {
		if (testAbilityFlag) {
			Debug.Log("TEST_Ability START");
			//AbsAbility.InitLinkedAbility(ref testAbility, ref testAb);
			Debug.Log("Ability Type:  " + testAb.GetType());
			object a = testAb.Effect();
			if (a != null) Debug.Log("return value:  " + (int)a);
			else Debug.Log("Error");
			Debug.Log("TEST_Ability END");
		}
	}


	public bool testBattleArmyFlag = true;
	public BattleArmy testBattleArmy;
	public Army sourceArmy;
	private void TEST_BattleArmy() {
		if (testBattleArmyFlag) {
			testBattleArmy.Create(sourceArmy);
		}
	}

	public bool testStatCalcFlag = true;
	public BattleGroup testBattleGroupStatCalc;
	private void TEST_StatCalc() {
		if (testStatCalcFlag){
			Debug.Log("TEST_StatCalc START");
			Debug.Log("Health Max Value:  " + testBattleGroupStatCalc.HealthMaxCalc(null) );
			Armor tmpArmor = testBattleGroupStatCalc.ArmorCalc(null);
			Debug.Log("Armor Value:  " + tmpArmor.armorValue);
			Debug.Log("Armor Type:  " + tmpArmor.armorType);
			testBattleGroupStatCalc.ActStatAllocation(EnumApplicationTagType.healthActBase, "Health Act (battle)");
			Debug.Log("TEST_StatCalc END");
		}
	}

	// Update is called once per frame
	bool ft = true;
	void Update () {		
		if(ft) {
			TEST_Ability();
			TEST_BattleArmy();
			TEST_StatCalc();
			

			ft = false;
		}

		
	}


}


/* RISULTATI "STRESS TEST"
 * ipotizando di creare ad ogni update un battlegroup che possiede 150 abilità diverse e di allocare 13 abilità diverse per ogni battlegroup crato in questo modo
 * il tempo che intercorre tra un update e l'altro supera in maniera stabile gli 0.1 secondi quando vengono allocati oltre 5300 oggetti
 * arrivando a 0.2376349 secondi a 12200 oggetti e a 0.333333 a 20000
 * si ottengono gli stessi identici risultati se si chiede il calcolo del armor di ogni gruppo dopo averlo allocato (calcolo che in questo caso prende in considerazione oltre 140 delle 150 abilità del gruppo)
 * eseguendo lo stesso identico test allocando 1000 oggetti ad ogni update al posto di 100 si può notare che mentre tra 0 e 2000 oggetti il tempo tra ogni turno è di 0.02 secondi dai 3000 in su il tempo rimane fisso a 0.333333 (valore testao fino a 20000 oggetti)
 * di seguito viene inserito il codice finale del test
 int n = 0;								//n è dichiarata fuori dalla funzione Update cosi che non venga resettata di valore
 [...]
	int k = 1000;
	for (int i = 0; i < k; i++)
	{
		GameObject asd = GameObject.Instantiate<GameObject>(testBattleGroupStatCalc.gameObject);
		asd.GetComponent<BattleGroup>().ArmorCalc(null);
	}
	n++;
	Debug.Log(n * k);
	Debug.Log(Time.deltaTime);
 */
