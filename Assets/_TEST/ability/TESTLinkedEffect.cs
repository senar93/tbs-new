﻿using UnityEngine;
using System.Collections;
using System;

public class TESTLinkedEffect : AbsAbilityLinkedEffect {

	public override object Effect(object target = null, AbilitySender sender = null, AbsEventTagType eventTag = null, AbsAbilityValue baseAbilityValue = null, AbsAbilityValue specificAbilityValue = null, AbsAbilityValue[] battleAbilityValue = null, string optionalParameter = "")
	{
		if (specificAbilityValue != null) {
			Debug.Log("specificAbilityValue Type:  " + specificAbilityValue.GetType());
			if (specificAbilityValue is TESTSpecificValue) {
				TESTSpecificValue tmp = specificAbilityValue as TESTSpecificValue;
				return tmp.n;
			}
		}

		return null;
	}

	public new AbsEventTagType GetEventTagType(object target = null, AbilitySender sender = null, AbsAbilityValue baseAbilityValue = null, AbsAbilityValue specificAbilityValue = null, AbsAbilityValue[] battleAbilityValue = null, string optionalParameter = "")
	{
		//in un caso come questo la funzione non viene ridefinita (gia di suo normalmente restituisce null, è stata ridefinita solo come esempio
		//solo per provare, non viene usato in questo test, altrimenti ipotizando per esempio che questa abilità aumenti l'attacco di chi la possiede di N
		//  dovrebbe restituire un valore che faccia capire che questa abilità non genera nessun evento (null potrebbe essere interpretato in questo modo)
		return null;
	}

}
